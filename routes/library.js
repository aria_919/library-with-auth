const express = require('express');
const router = express.Router();
const libraryController = require('../api/controllers/library');
router.get('/', libraryController.getAll);
router.post('/', libraryController.create);
router.get('/:libraryId', libraryController.getById);
router.get('/search/:title', libraryController.getByTitle);

router.put('/:libraryId', libraryController.updateById);
router.delete('/:libraryId', libraryController.deleteById);
module.exports = router;