const libraryModel = require('../models/Library');

module.exports = {
  getById: function (req, res, next) {
    console.log(req.body);
    libraryModel.findById(req.params.bookId, function (err, bookInfo) {
      if (err) {
        next(err);
      } else {
        res.json({
          status: "success",
          message: "Book found!!!",
          data: {
            books: bookInfo
          }
        });
      }
    });
  },
  getByTitle: function (req, res, next) {
    console.log(req.body);
    libraryModel.find({title:req.params.title}, function (err, bookInfo) {
      if (err) {
        next(err);
      } else {
        res.json({
          status: "success",
          message: "Book found!!!",
          data: {
            books: bookInfo
          }
        });
      }
    });
  },
  getAll: function (req, res, next) {
    let booksList = [];
    libraryModel.find({}, function (err, books) {
      if (err) {
        next(err);
      } else {
        for (let book of books) {
          //if we want to return only specific values to client
          booksList.push({
            id: book._id,
            name: book.title
          });
        }
        res.json({
          status: "success",
          message: "Books list found!!!",
          data: {
            books: booksList
          }
        });

      }
    });
  },
  updateById: function (req, res, next) {
    libraryModel.findByIdAndUpdate(req.params.bookId, {
      title: req.body.title
    }, function (err, bookInfo) {
      if (err)
        next(err);
      else {
        res.json({
          status: "success",
          message: "Book updated successfully!!!",
          data: null
        });
      }
    });
  },
  deleteById: function (req, res, next) {
    libraryModel.findByIdAndRemove(req.params.bookId, function (err, bookInfo) {
      if (err)
        next(err);
      else {
        res.json({
          status: "success",
          message: "Book deleted successfully!!!",
          data: null
        });
      }
    });
  },
  create: function (req, res, next) {
    libraryModel.create({
      title: req.body.title
    }, function (err, result) {
      if (err)
        next(err);
      else
        res.json({
          status: "success",
          message: "Book added successfully!!!",
          data: null
        });

    });
  },
}