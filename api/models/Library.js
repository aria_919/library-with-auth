const mongoose = require('mongoose');
//Define a schema
const Schema = mongoose.Schema;
const LibrarySchema = new Schema({
    title: {
        type: String,
        trim: true,
        required: true,
    },
    authors: {
        type: Array,
    },
    publisher: {
        type: String
    },
    price: {
        type:Number
    }
});
module.exports = mongoose.model('Library', LibrarySchema);